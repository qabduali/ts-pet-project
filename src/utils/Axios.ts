import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "https://kinopoiskapiunofficial.tech/api/",
  timeout: 2000,
  headers: {
    "X-API-KEY": "084f72d9-c713-46f2-8320-51dff3e7800a",
    "Content-Type": "application/json",
  },
});

export default axiosInstance;