import HomePage from "./pages/HomePage";
import { Routes, Route } from "react-router-dom";
import LoginPage from "./pages/LoginPage";
import { MyListPage } from "./pages/MyListPage";
import MovieInfo from "./components/MovieInfo/MovieInfo";
import MovieInfoPage from "./pages/MovieInfoPage";
import NotFoundPage from "./pages/NotFoundPage";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<HomePage />}></Route>
        <Route path="/login" element={<LoginPage />}></Route>
        <Route path="/list" element={<MyListPage />}></Route>
        <Route path="movie" element={<MovieInfoPage/>}>
          <Route path=":movieId" element={<MovieInfo/>}></Route>
        </Route>
        <Route path="*" element={<NotFoundPage/>}/>
      </Routes>
    </div>
  );
}

export default App;
