import "./FilmItemCell.css";
import { IReleasesData, IPremiersData } from "../Interfaces/IFilmsData";
import { Link } from "react-router-dom";
import { IMovieInfo } from "../MovieInfo/MovieInfo";

type TFilmItemCellProps = { movieProps: IReleasesData | IPremiersData | IMovieInfo};

const FilmItemCell: React.FC<TFilmItemCellProps> = ({ movieProps }) => {
  return (
    <Link
      to={`/movie/${
        "kinopoiskId" in movieProps ? movieProps.kinopoiskId : movieProps.filmId
      }`}
    >
      <div
        className="container"
        id={
          "kinopoiskId" in movieProps
            ? movieProps.kinopoiskId.toString()
            : movieProps.filmId.toString()
        }
      >
        <div>
          {"rating" in movieProps && movieProps.rating ? (
            <div className="rating">{movieProps.rating.toFixed(1)}</div>
          ) : null}
          <img src={movieProps.posterUrl} alt="Picture not Loaded"></img>
        </div>
        <div className="filmName">
          {movieProps.nameRu ? movieProps.nameRu : movieProps.nameEn}
        </div>
        <div className="genres">
          <>
            {movieProps.genres.length > 1
              ? `${movieProps.genres[0].genre}, ${movieProps.genres[1].genre}`
              : `${movieProps.genres[0].genre}`}
          </>
        </div>
      </div>
    </Link>
  );
};

export default FilmItemCell;
