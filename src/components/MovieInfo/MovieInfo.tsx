import { useEffect, useState } from "react";
import axiosInstance from "../../utils/Axios";
import { IFilmsData } from "../Interfaces/IFilmsData";
import Button from "@mui/material/Button";
import "./MovieInfo.css";
import {
  TableContainer,
  Table,
  TableBody,
  TableCell,
  TableRow,
} from "@mui/material";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { LinearProgress } from "@mui/material";

export interface IMovieInfo extends IFilmsData {
  nameOriginal: string | null;
  ratingKinopoisk: number;
  ratingKinopoiskVoteCount: number;
  description: string;
  kinopoiskId: number;
  filmLength: number;
  filmId: number;
}

const MovieInfo: React.FC<any> = ({ propId }) => {
  const [movieInfo, setMovieInfo] = useState<IMovieInfo>();
  const [loading, setLoading] = useState<boolean>(true);
  const [isAdded, setIsAdded] = useState<boolean>(false);

  const getFilmInfo = async (id: string) => {
    const response = await axiosInstance.get(`v2.2/films/${id}`);
    console.log(response.data);
    setMovieInfo(response.data);
    setLoading(false);
  };

  const handleAdd = () => {
    toast("🦄 You have added the film in your list", {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    setIsAdded(true);
    if (localStorage.getItem("addedFilms")?.length) {
      const addedFilms = JSON.parse(localStorage.getItem("addedFilms")!);
      addedFilms.push(propId);
      localStorage.setItem("addedFilms", JSON.stringify(addedFilms));
    } else {
      const addedFilms: (string | number)[] = [propId];
      localStorage.setItem("addedFilms", JSON.stringify(addedFilms));
    }
    localStorage.setItem(`${propId}`, JSON.stringify(movieInfo));
  };

  const handleRemove = () => {
    toast("🦄 You have removed the film from your list", {
      position: "top-right",
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    if (localStorage.getItem("addedFilms")?.length) {
      const addedFilms = JSON.parse(localStorage.getItem("addedFilms")!);
      const newAddedFilms = addedFilms.filter(
        (elem: string) => elem !== propId
      );
      console.warn(newAddedFilms);
      localStorage.setItem("addedFilms", JSON.stringify(newAddedFilms));
    }
    setIsAdded(false);
    localStorage.removeItem(`${propId}`);
  };

  useEffect(() => {
    getFilmInfo(propId);
    localStorage.getItem(propId) ? setIsAdded(true) : null;
  }, []);

  const style = {
    borderBottom: "none",
    paddingLeft: "0",
    paddingRight: "40px",
    color: "white",
    verticalAlign: "top"
  };

  if (loading) {
    return (
      <>
        <LinearProgress variant="indeterminate" color="inherit" />
      </>
    );
  }

  return (
    <>
      {!loading && (
        <div className="movie-container">
          <div className="poster">
            <img
              className="movie-poster"
              src={movieInfo!.posterUrl}
              alt="Picture not Loaded"
            />
          </div>
          <div className="description">
            <h2>
              {movieInfo!.nameRu} ({movieInfo!.year})
            </h2>
            {movieInfo!.nameOriginal ? (
              <h4>{movieInfo!.nameOriginal}</h4>
            ) : null}
            {!isAdded ? (
              <Button
                variant="contained"
                size="small"
                style={{ marginBottom: "10px", marginTop: "10px" }}
                onClick={handleAdd}
              >
                Add to my list
              </Button>
            ) : (
              <Button
                variant="contained"
                size="small"
                color="secondary"
                style={{ marginBottom: "10px", marginTop: "10px" }}
                onClick={handleRemove}
              >
                Remove from my list
              </Button>
            )}
            <ToastContainer />
            <h3> About the Movie </h3>
            <TableContainer id="table">
              <Table>
                <TableBody>
                  <TableRow>
                    <TableCell {...{ style }}>Production year: </TableCell>
                    <TableCell {...{ style }}>{movieInfo!.year}</TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell {...{ style }}>Country: </TableCell>
                    <TableCell {...{ style }}>
                      {movieInfo!.countries[0].country}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell {...{ style }}>Genres: </TableCell>
                    <TableCell {...{ style }}>
                      {movieInfo!.genres.map((elem) => {
                        return `${elem.genre} `;
                      })}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell {...{ style }}>Description: </TableCell>
                    <TableCell {...{ style }}>
                      {movieInfo!.description}
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell {...{ style }}>Duration: </TableCell>
                    <TableCell {...{ style }}>
                      {movieInfo!.filmLength} minutes
                    </TableCell>
                  </TableRow>
                  <TableRow>
                    <TableCell {...{ style }}>Rating: </TableCell>
                    <TableCell {...{ style }}>
                      {movieInfo!.ratingKinopoisk} /{" "}
                      {movieInfo!.ratingKinopoiskVoteCount} votes
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        </div>
      )}
    </>
  );
};

export default MovieInfo;
