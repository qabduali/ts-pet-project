export interface IFilmsData {
  countries: Array<{ country: string }>;
  duration: number;
  genres: Array<{ genre: string }>;
  nameEn: string;
  nameRu: string;
  posterUrl: string;
  posterUrlPreview: string;
  year: number;
}
export interface IReleasesData extends IFilmsData {
  expectationsRating: number;
  expectationsRatingVoteCount: number;
  filmId: number;
  rating: number;
  ratingVoteCount: number;
  releaseDate: string;
}

export interface IPremiersData extends IFilmsData {
  kinopoiskId: number;
  premiereRu: Date;
}
