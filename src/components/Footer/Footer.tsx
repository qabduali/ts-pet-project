import TwitterIcon from "@mui/icons-material/Twitter";
import FacebookIcon from "@mui/icons-material/Facebook";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import GoogleIcon from "@mui/icons-material/Google";
import InstagramIcon from "@mui/icons-material/Instagram";
import "./Footer.css";

const Footer = () => {
  return (
    <div className="footer">
      <h3>Online Cinema KinoMan</h3>
      <p>
        KinoMan is an online platform that allows you to watch any movie from
        any device.
      </p>
      <p>Developed by Damir Kabdualiyev, Copyright 2022.</p>
      <div className="icons-container">
        <TwitterIcon></TwitterIcon>
        <FacebookIcon></FacebookIcon>
        <LinkedInIcon></LinkedInIcon>
        <GoogleIcon></GoogleIcon>
        <InstagramIcon></InstagramIcon>
      </div>
    </div>
  );
};

export default Footer;
