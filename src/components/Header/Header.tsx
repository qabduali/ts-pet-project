import "./Header.css";
import HomeIcon from "@mui/icons-material/Home";
import AddBoxIcon from "@mui/icons-material/AddBox";
import { NavLink } from "react-router-dom";

const Header = () => {
  return (
    <div className="header">
      <div className="nav-cell">
        <NavLink to="/">
          <span className="nav-text">KinoMan </span>
          <HomeIcon fontSize="inherit" />
        </NavLink>
      </div>
      <div className="nav-cell2">
        <NavLink to="/list">
          <span className="nav-text">My List </span>
          <AddBoxIcon fontSize="inherit" />
        </NavLink>
      </div>
    </div>
  );
};

export default Header;
