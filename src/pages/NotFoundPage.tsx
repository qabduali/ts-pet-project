import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import { CenterFocusStrong } from "@mui/icons-material";

const NotFoundPage = () => {
  return (
    <>
        <Header/>
        <div style={{
          textAlign: "center",
          color: "white",
          height: "300px",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center", 
          backgroundColor: "rgb(43, 41, 41)"
        }}>
            <h1>404 Page Not Found</h1>
        </div>
        <Footer/>
    </>
  )
}

export default NotFoundPage