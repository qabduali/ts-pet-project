import Header from "../components/Header/Header";
import { useEffect, useState } from "react";
import FilmItemCell from "../components/FilmItemCell/FilmItemCell";
import "./styles/homePageStyle.css";
import Footer from "../components/Footer/Footer";
import { LinearProgress } from "@mui/material";
import {
  IReleasesData,
  IPremiersData,
} from "../components/Interfaces/IFilmsData";
import axiosInstance from "../utils/Axios";

const HomePage: React.FC = () => {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [releases, setReleases] = useState<IReleasesData[]>([]);
  const [premiers, setPremiers] = useState<IPremiersData[]>([]);

  const getReleases = async () => {
    const response = await axiosInstance.get("v2.1/films/releases", {
      params: {
        year: 2022,
        month: "APRIL",
        page: 1,
      },
    });
    setReleases(response.data.releases);
  };

  const getPremiers = async () => {
    const response = await axiosInstance.get("v2.2/films/premieres", {
      params: {
        year: 2022,
        month: "MAY",
      },
    });
    setPremiers(response.data.items);
    setIsLoading(false);
  };

  useEffect(() => {
    getReleases();
    getPremiers();
  }, []);

  if (isLoading) {
    return (
      <>
        <Header />
        <LinearProgress variant="indeterminate" color="inherit" />
        <Footer />
      </>
    );
  }

  return (
    <>
      <Header />
      <div className="releases">
        <h2>Top Releases</h2>
        <h3>Available Only in KinoMan</h3>
        <div className="releases-box">
          <div className="releases-container">
            {releases.map((element: IReleasesData) => {
              return (
                <FilmItemCell
                  movieProps={element}
                  key={element.posterUrl}
                ></FilmItemCell>
              );
            })}
          </div>
        </div>
      </div>
      <div className="premiers">
        <h2>Greatest Premiers</h2>
        <h3>Enjoy with KinoMan</h3>
        <div className="premiers-box1">
          <div className="premiers-containter">
            {premiers.map((element: IPremiersData) => {
              return element.kinopoiskId % 3 === 0 ? (
                <FilmItemCell
                  movieProps={element}
                  key={element.posterUrl}
                ></FilmItemCell>
              ) : null;
            })}
          </div>
        </div>
      </div>
      <div className="premiers">
        <h2>Recommendations for You</h2>
        <div className="premiers-box1">
          <div className="premiers-containter">
            {premiers.map((element: IPremiersData) => {
              return element.kinopoiskId % 3 !== 0 ? (
                <FilmItemCell
                  movieProps={element}
                  key={element.posterUrl}
                ></FilmItemCell>
              ) : null;
            })}
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default HomePage;
