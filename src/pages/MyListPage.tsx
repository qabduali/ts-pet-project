import Footer from "../components/Footer/Footer";
import Header from "../components/Header/Header";
import { useState, useEffect } from "react";
import { IMovieInfo } from "../components/MovieInfo/MovieInfo";
import FilmItemCell from "../components/FilmItemCell/FilmItemCell";
import "./styles/MyListPage.css";

export const MyListPage = () => {
  const [addedMovies, setAddedMovies] = useState<IMovieInfo[]>();

  useEffect(() => {
    if (localStorage.getItem("addedFilms")?.length) {
      const items = JSON.parse(localStorage.getItem("addedFilms")!);
      const newLolo = items.map((element: string) => {
        return JSON.parse(localStorage.getItem(element)!);
      });
      setAddedMovies(newLolo);
    }
  }, []);

  return (
    <>
      <Header />
      <div className="my-list">
        <h2>Movies in your list</h2>
        <div className="mylist-box">
          <div className="mylist-container">
            {addedMovies?.length ? (
              addedMovies.map((elem: IMovieInfo) => {
                return (
                  <FilmItemCell movieProps={elem} key={elem.kinopoiskId} />
                );
              })
            ) : (
              <h3>There are no movies yet</h3>
            )}
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};
