import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import MovieInfo from "../components/MovieInfo/MovieInfo";
import { useParams } from "react-router-dom";

const MovieInfoPage = () => {
    const params = useParams();
    const id: string = params.movieId!;
  return (
    <>
      <Header />
      <MovieInfo propId={id}/>
      <Footer />
    </>
  );
};

export default MovieInfoPage;
